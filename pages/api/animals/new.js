import { withIronSessionApiRoute } from "iron-session/next"
import { sessionConfig } from "../../../logic/session"

export default withIronSessionApiRoute(async function login(req, res) {

    const response = await fetch('http://127.0.0.1:9200/animals/_doc', {
        method: 'POST',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(req.body),
    })

    fetch('http://127.0.0.1:9200/animals/_refresh/', { method: 'POST' })
    
    const data = await response.json()

    return res.status(200).json(data)
}, sessionConfig)
