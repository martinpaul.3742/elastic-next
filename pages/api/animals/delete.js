import { withIronSessionApiRoute } from "iron-session/next"
import { sessionConfig } from "../../../logic/session"

export default withIronSessionApiRoute(async function remove(req, res) {

    const response = await fetch('http://127.0.0.1:9200/animals/_doc/' + req.body, {
        method: 'DELETE'
    })

    fetch('http://127.0.0.1:9200/animals/_refresh/', { method: 'POST' })
    
    const data = await response.json()

    return res.status(200).json(data)
}, sessionConfig)
