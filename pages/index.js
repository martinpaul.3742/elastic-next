// import Head from 'next/head'
// import Image from 'next/image'
// import styles from '../styles/Home.module.css'
import { useRouter } from 'next/router'
import { useState } from 'react'


export async function getServerSideProps() {
  const response = await fetch('http://127.0.0.1:9200/animals/_search')
  const json = await response.json()

  return { props: { data: json } }
}

export async function handleSubmit(e, refreshData) {  
  e.preventDefault()
  
  let data = new FormData(e.currentTarget)
    data = Object.fromEntries(data)

  const response = await fetch('/api/animals/new', {
    method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(data),
  }),
  json = await response.json()

  if (json?._id) {
    refreshData()
  }
}

export async function deleteAnimal(e, refreshData) {  
  let id = e.currentTarget.getAttribute('data-id')

  const response = await fetch('/api/animals/delete', {
    method: 'POST',
		headers: {
			'Accept': 'application/json',
			'Content-Type': 'application/json',
		},
		body: JSON.stringify(id),
  }),
  json = await response.json()

  if (json?._id) {
    refreshData()

    ////// start test //////
    fetch('http://127.0.0.1:9200/animals/_refresh/', { method: 'POST' })
    const test = await fetch('/api/animals', {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify(id),
    })
    console.log(await test.json())
    ////// end test //////
  }
}

export async function searchGif(e, setGifResults) {
  setGifResults([])
  const API_KEY = 'RhdAQtlrDuFrjRWpqzebv3JjEyd7BnXR'
  const query = e.currentTarget.value
  if (query) {
      const response = await fetch(`https://api.giphy.com/v1/gifs/search?api_key=${API_KEY}&q=${query}`)
      const results = await response.json()
      console.log(query, results.data)
      if (results) setGifResults(results.data)
  }
}

export function selectGif(e, setSelectedGif, setGifResults) {
  const url = e.currentTarget.getAttribute('src')
  const searchInput = e.currentTarget.closest('form').querySelector('input[name="gifSearch"]')
  setSelectedGif(url)
  setGifResults([])
  searchInput.value = ''
}

export default function Home({ data }) {
  const router = useRouter()
  const [animals, setAnimals] = useState(data?.hits?.hits)
  const [gifResults, setGifResults] = useState('')
  const [selectedGif, setSelectedGif] = useState([])

  console.log(data?.hits?.hits)
  
  const refreshData = () => {
    console.log('refresh')
    router.replace(router.asPath)
  }

  return (
    <div id='page' className='container mx-auto'>
      <div className='row'>
        <div className='col'>
          <div className='card'>
            <h2>Ajouter un animal</h2>
            <form onSubmit={(e) => handleSubmit(e, refreshData)}>
              <div>
                <label htmlFor="form-name">Nom</label>
                <input id="form-name" name="name" type="text" placeholder="Nom de l'animal" />
              </div>
              <div>
                <label htmlFor="form-contient">Continent</label>
                <select id="form-contient" name="continent">
                  <option value="africa">Afrique</option>
                  <option value="asia">Asie</option>
                  <option value="europe">Europe</option>
                  <option value="latin-america">Amérique latine</option>
                  <option value="north-america">Amérique du nord</option>
                  <option value="oceania">Océanie</option>
                </select>
              </div>
              <div style={{display:'flex'}}>
                <input id="form-mammel" name="mammel" type="checkbox" />
                <label htmlFor="form-mammel">Mammifère ?</label>
              </div>
              <div>
                <label>GIF</label>
                <input name="gifSearch" type="text" placeholder='Chercher un GIF' onChange={(e) => searchGif(e, setGifResults)} />
                <input name="gif" type="hidden" value={selectedGif} onChange={() => null} />
                {gifResults.length > 0 && (
                  <div className='container-gif-results'>
                    {gifResults.map(result => {
                      return <img key={result.id} width="100%" src={result.images.fixed_width_small.url} onClick={(e) => selectGif(e, setSelectedGif, setGifResults)} />
                    }) }
                  </div>
                )}
                {selectedGif && (
                  <img className='mt-4' width="100%" src={selectedGif} />
                )}
              </div>
              <button type='submit'>Enregistrer</button>
            </form>
          </div>
        </div>
        <div className='col'>
          {data?.hits?.hits && data?.hits?.hits.map((animal, index) => {
            if (index > 10) return
            return (
              <div key={animal._id} className="card">
                <h4>{animal._source.name}</h4>
                <ul>
                  <li style={{textTransform: 'capitalize'}}>{animal._source.continent}</li>
                  {animal._source.mammel && <li>✅ Mammifère</li>}
                </ul>
                <img src={animal._source.gif} />
                <div><a href='#0' data-id={animal._id} onClick={(e) => confirm('Êtes-vous sûr de vouloir supprimer cet animal ?') && deleteAnimal(e, refreshData)}>Supprimer</a></div>
              </div>
            )
          })}
        </div>
      </div>
    </div>
  )
}
